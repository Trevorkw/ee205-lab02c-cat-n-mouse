///////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab02c - Cat `n Mouse - EE 205 - Spr 2022
///
/// This is a classic "I'm thinking of a number" guessing game.  The mouse
/// will think of a number... and the cat will keep trying to guess it.
///
/// @file    catNmouse.c
/// @version 1.0 - Initial version
///
/// Compile: $ gcc -o catNmouse catNmouse.c
///
/// Usage:  catNmouse [n]
///   n:  The maximum number used in the guessing game
///
/// Exit Status:
///   1:  The command line parameter was not a valid value
///   0:  The cat finally guessed correctly
///
/// Example:
///   $ ./catNmouse 2000
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1385
///   No cat... the number I’m thinking of is smaller than 1385
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1382
///   No cat... the number I’m thinking of is larger than 1382
///   OK cat, I'm thinking of a number from 1 to 2000.  Make a guess:  1384
///   You got me.
///   |\---/|
///   | o_o |
///    \_^_/
///   
/// @author  @Trevor Chang <@trevorkw@hawaii.edu>
/// @date    @ 25 Jan 2022
///////////////////////////////////////////////////////////////////////////////


#include <stdio.h>
#include <stdlib.h>
#define DEFAULT_MAX_NUMBER 2048

int main( int argc, char* argv[] ) {
   printf( "Cat `n Mouse\n" );
   printf( "The number of arguments is: %d\n", argc );

   // This is an example of getting a number from a user
   // Note:  Don't enter unexpected values like 'blob' or 1.5.  
   
   int aGuess, theNumberImThinkingOf, yesno, maxnumber;

   printf("Would you like to select your own max value?  [y/n]:\n");
   scanf("%c", &yesno);
   if(yesno == 'y'){
      printf("Type your desired max value:  ");
      scanf("%d", &maxnumber);
      printf("Max number will be set to %d\n", maxnumber);
   }
   else if(yesno == 'n'){
      printf("Max number will be set to 2048\n");
          maxnumber = DEFAULT_MAX_NUMBER;
   }
   else{
         printf("Type in 'y' or 'n' nextime");
         return 0;
   }

    theNumberImThinkingOf = rand()%maxnumber + 1;

   printf("OK cat, I'm thinking of a number from 1 to %d.  Make a guess:  ", maxnumber);
   scanf("%d", &aGuess);
   do{
      if(aGuess < 1){
         printf("You must enter a number that’s >= 1\n");
         printf( "Ok cat, I'm thinking of a number from 1 to %d.  Make a guess: ", maxnumber );
         scanf("%d", &aGuess);}

      if(aGuess > maxnumber){
         printf("You must enter a number that’s <= %d\n", maxnumber);
         printf( "Ok cat, I'm thinking of a number from 1 to %d.  Make a guess: ", maxnumber );
         scanf("%d", &aGuess);}

      if(aGuess > theNumberImThinkingOf){
         printf("No cat... the number I’m thinking of is smaller than %d\n", aGuess);
         printf( "Ok cat, I'm thinking of a number from 1 to %d.  Make a guess: ", maxnumber );
         scanf("%d", &aGuess);}

      if(aGuess < theNumberImThinkingOf){
         printf("No cat... the number I’m thinking of is greater than %d\n", aGuess);
         printf( "Ok cat, I'm thinking of a number from 1 to %d.  Make a guess: ", maxnumber );
         scanf("%d", &aGuess);}
       
   }while(aGuess != theNumberImThinkingOf);
   
   if(aGuess == theNumberImThinkingOf){
      printf("You got me.\n /\\_/\\ \n( o.o )\n> ^ <\n");
   }

   return 1;  // This is an example of how to return a 1
}

